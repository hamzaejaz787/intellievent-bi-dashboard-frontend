import { BrowserRouter, Route, Routes } from "react-router-dom";

import Dashboard from "./Dashboard";
import Navbar from "./components/Navbar";
import { ThemeProvider } from "./components/theme-provider";
import Search from "./components/Search";
import ThemeToggle from "./components/ThemeToggle";
import ClientInfo from "./ClientInfo";
import jsonData from "./data/insights.json";

function App() {
  const getRecentJobs = jsonData.recent_jobs;

  return (
    <>
      <ThemeProvider defaultTheme="dark" storageKey="vite-ui-theme">
        <BrowserRouter>
          <div className="flex justify-between items-center ">
            <div className="border-b w-full flex justify-between items-center space-x-4 px-4">
              <div className="flex h-16 items-center p-4">
                <Navbar />
              </div>
              <Search />
              <ThemeToggle />
            </div>
          </div>
          <Routes>
            <Route path="/" element={<Dashboard />} />
            <Route
              path="/client/:id"
              element={<ClientInfo data={getRecentJobs} />}
            />
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
    </>
  );
}

export default App;
