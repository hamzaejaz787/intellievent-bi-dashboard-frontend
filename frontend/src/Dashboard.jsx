import InfoTabs from "./components/InfoTabs";
import RecentJobs from "./components/RecentJobs";
import jsonData from "./data/insights.json";

const Dashboard = () => {
  const getRecentJobs = jsonData.recent_jobs;

  return (
    <>
      {/* <div className="flex gap-4 flex-col md:flex-row p-4 md:p-8 justify-between"> */}
      <InfoTabs />
      {/* <RecentJobs recentJobs={getRecentJobs} /> */}
      {/* </div> */}
    </>
  );
};

export default Dashboard;
