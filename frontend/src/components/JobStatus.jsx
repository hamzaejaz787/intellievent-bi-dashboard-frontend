import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "./ui/dropdown-menu";
import pieIcon from "../assests/pie-icon.svg";
import { ChevronDown } from "lucide-react";
import { Button } from "./ui/button";
import JobsChart from "./charts/JobsChart";

const JobStatus = () => {
  return (
    <>
      <div className="block shadow-md rounded-lg p-8 space-y-8 border w-full my-8 mx-auto">
        <div className="flex justify-between items-center">
          <div className="flex items-center gap-2">
            <img src={pieIcon} alt="" />
            <h2 className="text-xl font-medium">Job Status </h2>
          </div>

          <DropdownMenu>
            <DropdownMenuTrigger>
              <Button variant="outline" className="text-md">
                Warehouse <ChevronDown />
              </Button>
            </DropdownMenuTrigger>
            <DropdownMenuContent>
              <DropdownMenuItem className="cursor-pointer">
                Warehouse 1
              </DropdownMenuItem>
              <DropdownMenuItem className="cursor-pointer">
                Warehouse 2
              </DropdownMenuItem>
              <DropdownMenuItem className="cursor-pointer">
                Warehouse 3
              </DropdownMenuItem>
            </DropdownMenuContent>
          </DropdownMenu>
        </div>

        <div>
          <JobsChart />
        </div>
      </div>
    </>
  );
};

export default JobStatus;
