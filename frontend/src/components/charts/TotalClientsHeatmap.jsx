import MapGL, { Layer, Source } from "react-map-gl";
import jsonData from "../../data/insights.json";

const mapboxAccessToken = import.meta.env.VITE_MAPBOX_API;

const TotalClientsHeatmap = () => {
  //Heatmap
  const clientsByCountry = jsonData.clients_by_country;
  const countries = Object.keys(clientsByCountry);
  const countryCoordinates = {
    Australia: [-25.2744, 133.7751],
    Austria: [47.5162, 14.5501],
    Bahrain: [26.0667, 50.5577],
    Belgium: [50.8503, 4.3517],
    Brazil: [-14.235, -51.9253],
    Canada: [56.1304, -106.3468],
    China: [35.8617, 104.1954],
    Croatia: [45.1, 15.2],
    Cyprus: [35.1264, 33.4299],
    "Czech Republic": [49.8175, 15.473],
    Denmark: [56.2639, 9.5018],
    Egypt: [26.8206, 30.8025],
    Estonia: [58.5953, 25.0136],
    Finland: [61.9241, 25.7482],
    France: [46.6035, 1.8883],
    Germany: [51.1657, 10.4515],
    Greece: [39.0742, 21.8243],
    "Hong Kong": [22.3193, 114.1694],
    Hungary: [47.1625, 19.5033],
    Iceland: [64.9631, -19.0208],
    India: [20.5937, 78.9629],
    Indonesia: [-0.7893, 113.9213],
    Ireland: [53.4129, -8.2439],
    Israel: [31.0461, 34.8516],
    Italy: [41.8719, 12.5674],
    Japan: [36.2048, 138.2529],
    Jordan: [30.5852, 36.2384],
    Kenya: [-1.2921, 36.8219],
    Kuwait: [29.3759, 47.9774],
    Liechtenstein: [47.166, 9.5554],
    Lithuania: [55.1694, 23.8813],
    Malaysia: [4.2105, 101.9758],
    Malta: [35.9375, 14.3754],
    Mexico: [23.6345, -102.5528],
    Morocco: [31.7917, -7.0926],
    Netherlands: [52.3676, 4.9041],
    "New Zealand": [-40.9006, 174.886],
    Norway: [60.472, 8.4689],
    Oman: [21.4735, 55.9754],
    Philippines: [12.8797, 121.774],
    Poland: [51.9194, 19.1451],
    Portugal: [39.3999, -8.2245],
    Qatar: [25.2769, 51.52],
    "Russian Federation": [61.524, 105.3188],
    "Saudi Arabia": [23.8859, 45.0792],
    "Serbia and Montenegro": [44.0165, 21.0059],
    Singapore: [1.3521, 103.8198],
    Slovenia: [46.1512, 14.9955],
    "South Africa": [-30.5595, 22.9375],
    "South Korea": [35.9078, 127.7669],
    Spain: [40.4637, -3.7492],
    Sweden: [60.1282, 18.6435],
    Switzerland: [46.8182, 8.2275],
    Taiwan: [23.6978, 120.9605],
    Tunisia: [33.8869, 9.5375],
    "United Arab Emirates": [23.4241, 53.8478],
    "United Kingdom": [55.3781, -3.436],
    "United States": [37.0902, -95.7129],
  };

  const heatMapData = countries.map((country) => {
    const coordinates = countryCoordinates[country];
    const orders = clientsByCountry[country];

    return {
      type: "Features",
      geometry: {
        type: "Point",
        coordinates: [coordinates[1], coordinates[0]],
      },
      properties: {
        orders: orders,
      },
    };
  });

  const initialViewport = {
    latitude: 37.0902,
    longitude: -95.7129,
    zoom: 0.5,
  };
  return (
    <>
      {" "}
      <MapGL
        mapboxAccessToken={mapboxAccessToken}
        initialViewState={initialViewport}
        style={{ width: "100%", height: 500 }}
        mapStyle="mapbox://styles/mapbox/streets-v11"
      >
        <Source
          id="heatmap"
          type="geojson"
          data={{ type: "FeatureCollection", features: heatMapData }}
        >
          <Layer
            id="heatmap-layer"
            type="heatmap"
            source="heatmap"
            paint={{
              "heatmap-weight": ["get", "clients"],
              "heatmap-intensity": 1.8,
              "heatmap-color": [
                "interpolate",
                ["linear"],
                ["heatmap-density"],
                0,
                "rgba(255, 255, 178, 0)",
                0.2,
                "rgb(209, 229, 168)",
                0.4,
                "rgb(134, 191, 118)",
                0.6,
                "rgb(72, 140, 90)",
                0.8,
                "rgb(33, 102, 70)",
                1,
                "rgb(0, 68, 27)",
              ],
              "heatmap-radius": 25,
            }}
          />
        </Source>
      </MapGL>
    </>
  );
};

export default TotalClientsHeatmap;
