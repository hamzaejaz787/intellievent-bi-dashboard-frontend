import jsonData from "../../data/insights.json";
import MapGL, { Source, Layer } from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";

const mapboxAccessToken = import.meta.env.VITE_MAPBOX_API;

const OverdueHeatmap = () => {
  const overdueReturnsByCountries = jsonData.overdue_returns_by_country;
  const countries = Object.keys(overdueReturnsByCountries);
  const countryCoordinates = {
    Australia: [-25.2744, 133.7751],
    Austria: [47.5162, 14.5501],
    Belgium: [50.8503, 4.3517],
    Canada: [56.1304, -106.3468],
    China: [35.8617, 104.1954],
    Egypt: [26.8206, 30.8025],
    France: [46.6035, 1.8883],
    Germany: [51.1657, 10.4515],
    Greece: [39.0742, 21.8243],
    Hungary: [47.1625, 19.5033],
    India: [20.5937, 78.9629],
    Ireland: [53.1424, -7.6921],
    Italy: [41.8719, 12.5674],
    Kenya: [-1.2921, 36.8219],
    Mexico: [23.6345, -102.5528],
    Netherlands: [52.3676, 4.9041],
    Portugal: [39.3999, -8.2245],
    "Russian Federation": [61.524, 105.3188],
    "Saudi Arabia": [23.8859, 45.0792],
    Singapore: [1.3521, 103.8198],
    "South Africa": [-30.5595, 22.9375],
    Spain: [40.4637, -3.7492],
    Sweden: [60.1282, 18.6435],
    Switzerland: [46.8182, 8.2275],
    "United Arab Emirates": [23.4241, 53.8478],
    "United Kingdom": [55.3781, -3.436],
    "United States": [37.0902, -95.7129],
  };

  const heatMapData = countries.map((country) => {
    const coordinates = countryCoordinates[country];
    const orders = overdueReturnsByCountries[country];

    return {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [coordinates[1], coordinates[0]],
      },
      properties: {
        orders: orders,
      },
    };
  });

  // Mapbox initial viewport settings
  const initialViewport = {
    latitude: 37.0902,
    longitude: -95.7129,
    zoom: 0.5,
  };
  return (
    <>
      {" "}
      <MapGL
        mapboxAccessToken={mapboxAccessToken}
        initialViewState={initialViewport}
        style={{ width: "100%", height: 500 }}
        mapStyle="mapbox://styles/mapbox/streets-v11"
      >
        <Source
          id="heatmap"
          type="geojson"
          data={{ type: "FeatureCollection", features: heatMapData }}
        >
          <Layer
            id="heatmap-layer"
            type="heatmap"
            source="heatmap"
            paint={{
              "heatmap-weight": ["get", "returns"],
              "heatmap-intensity": 1.5,
              "heatmap-color": [
                "interpolate",
                ["linear"],
                ["heatmap-density"],
                0,
                "rgba(255, 255, 178, 0)",
                0.2,
                "rgb(254, 217, 118)",
                0.4,
                "rgb(253, 141, 60)",
                0.6,
                "rgb(252, 78, 42)",
                0.8,
                "rgb(227, 26, 28)",
                1,
                "rgb(178, 24, 43)",
              ],
              "heatmap-radius": 20,
            }}
          />
        </Source>
      </MapGL>
    </>
  );
};

export default OverdueHeatmap;
