import {
  Chart as ChartJS,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  Legend,
  Tooltip,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import jsonData from "../../data/insights.json";

ChartJS.register(
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  Legend,
  Tooltip
);

const statusLabels = [
  "Active",
  "Cancelled",
  "Invoiced",
  "Option",
  "Quote Only",
  "Tentative",
];

const statusColors = {
  Active: "#4CAF50",
  Cancelled: "#FF5252",
  Invoiced: "#2196F3",
  Option: "#FFC107",
  "Quote Only": "#FF9800",
  Tentative: "#9C27B0",
};

const transformData = (data) => {
  const offices = Array.from(
    new Set(
      Object.keys(data).map(
        (key) => key.replace(/['"]/g, "").replace(/[()]/g, "").split(", ")[0]
      )
    )
  );

  const datasets = statusLabels.map((status) => ({
    label: status,
    data: offices.map((office) => {
      const key = `('${office}', '${status}')`;
      const orderData = data[key] || 0;
      return orderData;
    }),
    backgroundColor: statusColors[status],
  }));

  return { labels: offices, datasets };
};

const jobsByWarehouse = jsonData.job_status_by_office;

const JobsChart = () => {
  const transformedData = transformData(jobsByWarehouse);
  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: true,
        position: "right",
      },
    },
  };

  return (
    <Bar
      data={transformedData}
      options={options}
      style={{ height: 350, width: "100%" }}
    />
  );
};

export default JobsChart;
