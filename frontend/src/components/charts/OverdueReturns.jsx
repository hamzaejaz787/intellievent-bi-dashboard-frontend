import { Line } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import jsonData from "../../data/insights.json";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

const OverdueReturns = () => {
  const overdueReturns = jsonData.overdue_returns_cumulative;
  const ordersByMonth = overdueReturns.reduce((acc, [date, order]) => {
    const month = (new Date(date).getMonth() + 1).toString().padStart(2, "0");
    acc[month] = (acc[month] || 0) + order;
    return acc;
  }, {});

  const months = Object.keys(ordersByMonth);
  const orders = Object.values(ordersByMonth);
  const reorderIndex = 2; // Number of months to move to the end
  const reorderedMonths = [
    ...months.slice(reorderIndex),
    ...months.slice(0, reorderIndex),
  ];
  const reorderedOrders = [
    ...orders.slice(reorderIndex),
    ...orders.slice(0, reorderIndex),
  ];

  const data = {
    labels: reorderedMonths,
    datasets: [
      {
        label: "Overdue Returns Over Time",
        data: reorderedOrders,
        fill: false,
        borderColor: "rgb(122, 28, 5)",
        tension: 0.5,
      },
    ],
  };
  const options = {
    responsive: true,
  };

  return <Line data={data} options={options} />;
};

export default OverdueReturns;
