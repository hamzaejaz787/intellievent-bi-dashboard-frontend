import { Link } from "react-router-dom";
import { Card, CardContent, CardTitle } from "./ui/card";
import ActiveJobs from "./charts/ActiveJobs";
import OverdueReturns from "./charts/OverdueReturns";
import TotalClients from "./charts/TotalClients";

const RecentJobs = ({ recentJobs, tabValue }) => {
  return (
    <>
      <div className="h-full md:min-w-[250px] p-5 rounded-md shadow-md flex-2">
        <div className="min-w-[400px] w-full py-8">
          {tabValue === "active-jobs" && <ActiveJobs />}
          {tabValue === "returns" && <OverdueReturns />}
          {tabValue === "clients" && <TotalClients />}
        </div>
        <h1 className="text-2xl mb-6">Recent Jobs</h1>

        <div className="rounded grid grid-cols-1 gap-4">
          {recentJobs.map((recentJob) => {
            const { id, clientId, clientName, countryName, officeName } =
              recentJob;

            return (
              <Link
                key={id}
                to={`/client/${clientId}`}
                className="hover:shadow-muted-foreground"
              >
                <Card className="p-4">
                  <CardTitle className="text-lg pb-1">{clientName}</CardTitle>
                  <CardContent className="p-0">
                    <h3>{officeName}</h3>
                    <span className="text-muted-foreground">
                      Location: {countryName}
                    </span>
                  </CardContent>
                </Card>
              </Link>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default RecentJobs;
