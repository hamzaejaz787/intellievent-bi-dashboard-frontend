import { useState } from "react";
import {
  Tabs,
  TabsContent,
  TabsList,
  TabsTrigger,
} from "../components/ui/tabs";

import {
  Card,
  CardContent,
  CardHeader,
  CardTitle,
} from "../components/ui/card";
import jsonData from "../data/insights.json";
import JobStatus from "./JobStatus";
import RecentJobs from "./RecentJobs";
import ActiveHeatmap from "./charts/ActiveHeatmap";
import OverdueHeatmap from "./charts/OverdueHeatmap";
import TotalClientsHeatmap from "./charts/TotalClientsHeatmap";

const tabData = [
  {
    id: "active",
    value: "active-jobs",
    cardTile: "Active Jobs",
    cardHeading: jsonData.active_orders,
    cardDescription: "Total number of active jobs",
  },
  {
    id: "overdue",
    value: "returns",
    cardTile: "Overdue Returns",
    cardHeading: jsonData.overdue_rtn,
    cardDescription: "Total number of overdue returns",
  },
  {
    id: "totalClients",
    value: "clients",
    cardTile: "Total Clients",
    cardHeading: jsonData.clients,
    cardDescription: "Total number of clients",
  },
];

const InfoTabs = () => {
  const [currentTab, setCurrentTab] = useState("active-jobs");
  const getRecentJobs = jsonData.recent_jobs;

  const handleTabChange = (value) => {
    setCurrentTab(value);
  };

  return (
    <>
      <div className="flex gap-4 flex-col md:flex-row p-4 md:p-8 justify-between">
        <Tabs
          defaultValue="active-jobs"
          className="space-y-4 flex flex-col max-w-[950px] w-full 2xl:max-w-[1000px] flex-1"
          onValueChange={handleTabChange}
        >
          <TabsList className="grid grid-cols-1 gap-4 items-baseline lg:gap-8 sm:grid-cols-3 bg-transparent h-auto">
            {tabData.map((tab) => (
              <TabsTrigger key={tab.id} value={tab.value}>
                <Card className="p-4 w-full">
                  <CardHeader className="p-0">
                    <CardTitle className="text-base">{tab.cardTile}</CardTitle>
                  </CardHeader>

                  <CardContent className="whitespace-pre-wrap p-0">
                    <h2 className="text-xl font-bold text-secondary-foreground">
                      {tab.cardHeading}
                    </h2>

                    <p className="text-sm">{tab.cardDescription}</p>
                  </CardContent>
                </Card>
              </TabsTrigger>
            ))}
          </TabsList>

          <TabsContent value="active-jobs">
            <ActiveHeatmap />
          </TabsContent>
          <TabsContent value="returns">
            <OverdueHeatmap />
          </TabsContent>
          <TabsContent value="clients">
            <TotalClientsHeatmap />
          </TabsContent>

          <JobStatus />
        </Tabs>

        <RecentJobs recentJobs={getRecentJobs} tabValue={currentTab} />
      </div>
    </>
  );
};

export default InfoTabs;
