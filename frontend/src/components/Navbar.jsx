import { Link } from "react-router-dom";
import { cn } from "../libs/utils";
import { AiOutlineMenu } from "react-icons/ai";
import { Sheet, SheetContent, SheetTrigger } from "./ui/sheet";

const navLinks = [
  { id: "overview", title: "Overview", href: "/" },
  { id: "customers", title: "Customers", href: "/customers" },
  { id: "products", title: "Products", href: "/products" },
  { id: "settings", title: "Settings", href: "/settings" },
];

const Navbar = () => {
  return (
    <Sheet>
      <SheetTrigger>
        <AiOutlineMenu size={28} className="transition-all duration-200" />
      </SheetTrigger>
      <SheetContent side="left" className="w-full">
        <nav className={cn("flex flex-col gap-8 mt-6")}>
          {navLinks.map((item) => (
            <li key={item.id} className="list-none">
              <Link
                to={item.href}
                className="text-lg px-2 font-medium text-primary transition-colors hover:text-secondary"
              >
                {item.title}
              </Link>
            </li>
          ))}
        </nav>
      </SheetContent>
    </Sheet>
  );
};

export default Navbar;
