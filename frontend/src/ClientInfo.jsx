import { useParams } from "react-router-dom";
import {
  Card,
  CardDescription,
  CardHeader,
  CardTitle,
} from "./components/ui/card";

const formatDate = (dateTimeString) => {
  const date = new Date(dateTimeString);

  const day = date.getDate().toString().padStart(2, "0");
  const month = (date.getMonth() + 1).toString().padStart(2, "0");
  const year = date.getFullYear();

  const formattedDate = `${day}/${month}/${year}`;

  return formattedDate;
};

const ClientInfo = ({ data }) => {
  const params = useParams();
  const clientId = parseInt(params.id);
  const client = data.find((item) => item.clientId === clientId);

  const {
    id,
    clientName,
    collection_date,
    contactEmail,
    countryName,
    contactName,
    createDate,
    delivery_date,
    dispatch_date,
    jobTotal,
    jobType,
    officeName,
    orderStatus,
    phone,
    rental_end_date,
    rental_start_date,
    stock_allocated_date,
  } = client;

  return (
    <Card className="max-w-[720px] mx-auto my-8">
      <CardHeader>
        <CardTitle className="text-2xl text-center pt-2">
          {clientName}
        </CardTitle>

        <h2 className="text-lg text-center py-1">{countryName}</h2>

        <div className="flex items-center justify-center flex-wrap gap-4">
          <span className="text-muted-foreground text-sm">Type: {jobType}</span>
          <span className="text-muted-foreground text-sm">
            Status: {orderStatus}
          </span>
          <span className="text-muted-foreground text-sm">
            Delivery Date: {formatDate(delivery_date)}
          </span>
          <span className="text-muted-foreground text-sm">
            Dispatch Date: {formatDate(dispatch_date)}
          </span>
        </div>
      </CardHeader>
    </Card>
  );
};

export default ClientInfo;
