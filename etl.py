import pyodbc
from datetime import date, datetime
from json import dumps, JSONDecodeError, loads
from os import makedirs
from urllib.parse import urljoin
from azure.storage.blob import BlobServiceClient, ContentSettings
import pandas as pd
from requests import post


class IeData:
    BASE_URL = 'https://webapi1.ielightning.net/'
    AUTH_EP = '/api/v1/Authentication/GetAccessToken'
    JOBS_LIST_EP = '/api/v1/Jobs/JobsList/List'
    CLIENT_LIST_EP = '/api/v1/Clients/List'
    CREDENTIALS_FILE = 'credentials.txt'
    
    AZURE_SERVER = 'tcp:intellievent-db.database.windows.net'
    AZURE_DATABASE = 'intellievent-db'
    AZURE_SERVER_USERNAME = 'intelli-db-1'
    AZURE_SERVER_PASSWORD = 'databasepass_1'
    AZURE_STORAGE_CONNECTION_STRING = 'DefaultEndpointsProtocol=https;AccountName=intellieventfunctionnew;AccountKey=F5gteJUii7YlAwTlN2PaxrCSf8GBSwmTXONOeCcC0MbItbD7Q/MZWyK/YCefgJ9KWEy8JD12YYKb+ASt0JwXXw==;EndpointSuffix=core.windows.net'
    AZURE_CONTAINER_NAME = 'intelliventblob'

    def __init__(self):
        self.timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
        self.headers = None
        self.jobs_list_json = None
        self.clients_list_json = None
        self.cleaned_jobs = None
        self.cleaned_clients = None
        self.insights = {}

  
    def get_header_with_auth(self):
        with open(IeData.CREDENTIALS_FILE, 'r') as file:
            credentials = file.readlines()

        ident = credentials[0].strip()
        username = credentials[1].strip()
        password = credentials[2].strip()

        payload = {
            "ident": ident,
            "username": username,
            "password": password
        }

        auth_token_response = post(url=urljoin(IeData.BASE_URL,
                                               IeData.AUTH_EP),
                                   json=payload)

        if auth_token_response.status_code != 200:
            raise Exception(f'''Failed to Authenticate:
                {auth_token_response.text}''')

        auth_json = auth_token_response.json()
        token = auth_json["accessToken"]

        self.headers = {
            "Authorization": f"Bearer {token}",
            "Content-Type": "application/json"
        }

    def get_jobs(self):
        if not self.headers:
            raise Exception("Headers not set. Authenticate first.")

        jobs_list_response = post(url=urljoin(IeData.BASE_URL,
                                              IeData.JOBS_LIST_EP),
                                  headers=self.headers,
                                  json={})

        if jobs_list_response.status_code != 200:
            raise Exception(f"Failed to fetch jobs: {jobs_list_response.text}")

        self.jobs_list_json = loads(jobs_list_response.text)

    def get_clients(self):
        if not self.headers:
            raise Exception("Headers not set. Authenticate first.")

        client_list_response = post(url=urljoin(IeData.BASE_URL,
                                                   IeData.CLIENT_LIST_EP),
                                    headers=self.headers,
                                    json={})

        if client_list_response.status_code != 200:
            raise Exception(f'''Failed to fetch clients:
                {client_list_response.text}''')

        chunks = []
        try:
            for chunk in client_list_response.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    chunks.append(chunk)
            response_content = b''.join(chunks)
            self.clients_list_json = loads(response_content.decode('utf-8'))
        except JSONDecodeError as e:
            print(f"JSON Decoding Error: {e}")

    #No use for now
    def to_blob(self):
        if not file_path: 
            raise Exception('File path not provided') 
        
        
        file_path = f'data/{ie_data.timestamp}/insights.json'

        #Azure blob
        blob_service_client = BlobServiceClient.from_connection_string(self.AZURE_STORAGE_CONNECTION_STRING)
        

        blob_client = blob_service_client.get_blob_client(
            container=self.AZURE_CONTAINER_NAME,
            blob=f"insights_{ie_data.timestamp}.json"
        )


        with open(file_path, 'rb') as data:
            blob_client.upload_blob(data, content_settings=ContentSettings(content_type='application/json'), blob_type='BlockBlob')


    @staticmethod
    def transform_df(df, relevant_columns, date_columns_info, columns_to_drop):
        df.drop(columns=columns_to_drop, errors='ignore', inplace=True)
        df.rename(columns=date_columns_info, inplace=True)
        for col in date_columns_info.values():
            df[col] = pd.to_datetime(df[col], errors='coerce')
        df.replace({'1900-01-01T00:00:00': pd.NaT, '': pd.NaT}, inplace=True)
        return df[relevant_columns + list(date_columns_info.values())]

    def clean_raw_jobs(self):
        if self.jobs_list_json is None:
            raise Exception("Jobs data not loaded.")

        jobs_df = pd.json_normalize(self.jobs_list_json['items'])
        columns_to_drop = ['beginDate3', 'beginDate6']
        jobs_df.drop(columns=columns_to_drop, errors='ignore', inplace=True)
        date_columns_info = {
            'beginDate0': 'stock_allocated_date',
            'beginDate1': 'dispatch_date',
            'beginDate2': 'delivery_date',
            'beginDate2_5': 'rental_start_date',
            'beginDate3_5': 'rental_end_date',
            'beginDate4': 'collection_date',
            'beginDate5': 'check_in_date'
        }
        jobs_df.rename(columns=date_columns_info, inplace=True)

        for col in date_columns_info.values():
            jobs_df[col] = pd.to_datetime(jobs_df[col], errors='coerce')

        jobs_df.replace({'1900-01-01T00:00:00': pd.NaT, '': pd.NaT},
                        inplace=True)

        relevant_columns = [
            'id', 'clientName', 'orderStatus', 'jobTotal', 'createDate',
            'countryName', 'stock_allocated_date', 'dispatch_date',
            'delivery_date', 'rental_start_date', 'rental_end_date',
            'collection_date', 'check_in_date',
            'officeName', 'jobType', 'salesperson', 'contactName',
            'contactEmail', 'phone', 'clientId'
        ]

        self.cleaned_jobs = jobs_df[relevant_columns]

    def clean_raw_clients(self):
        if self.clients_list_json is None:
            raise Exception("Clients data not loaded.")

        clients_df = pd.json_normalize(self.clients_list_json['items']).copy()

        relevant_columns = [
            'clientId', 'clientName', 'primaryEmail',
            'createDate', 'countryName',
            'managedBy'
        ]

        self.cleaned_clients = self.transform_df(clients_df,
                                                 relevant_columns,
                                                 {},
                                                 [])

    
    
    def connect_db(self):
        try:
            conn = pyodbc.connect(
                f'DRIVER={{ODBC Driver 18 for SQL Server}};'
                f'SERVER={self.AZURE_SERVER};'
                f'DATABASE={self.AZURE_DATABASE};'
                f'UID={self.AZURE_SERVER_USERNAME};'
                f'PWD={self.AZURE_SERVER_PASSWORD};'
                f'Connect Timeout=30;'
            )
            
            return conn
        except Exception as e:
            print(f'Error connecting to the database: {e}')
            raise

   
    def update_db(self, df, table_name):
        try:
            timestamps_cols = df.select_dtypes(include=['datetime64[ns]', 'object']).columns
            for col in timestamps_cols:
                df[col] = df[col].apply(
                    lambda x: x.strftime('%Y-%m-%dT%H:%M:%S') 
                                if isinstance(x, (pd.Timestamp, datetime)) and pd.notna(x) and not pd.isnull(x)
                                else (x[0].strftime('%Y-%m-%dT%H:%M:%S') 
                                        if isinstance(x, tuple) and len(x) > 0 and isinstance(x[0], (pd.Timestamp, datetime)) 
                                        and pd.notna(x[0]) and not pd.isnull(x[0]) 
                                        else str(x)
                                    if isinstance(x, tuple) 
                                    else str(x)
                                )
                )

            # Create a database connection
            conn = self.connect_db()


            # Delete existing records
            conn.execute(f'DELETE FROM {table_name}')
            cursor = conn.cursor()

            for _, row in df.iterrows():
                columns = ', '.join(row.index)
                query = f'INSERT INTO {table_name} ({columns}) VALUES ({", ".join(["?" for _ in range(len(row))])})'
                cursor.execute(query, tuple(row))


            conn.commit()
        finally:
            if conn:
                conn.close()


    def save_cleaned_data(self):
        if self.cleaned_jobs is None or self.cleaned_clients is None:
            raise Exception('''Cleaned data not available.
                Please run the cleaning methods first.''')

        self.update_db(self.cleaned_jobs, 'dbo.jobs')
        self.update_db(self.cleaned_clients, 'dbo.clients')

    def get_insights(self):
        if self.cleaned_jobs is None or self.cleaned_clients is None:
            raise Exception("Cleaned data not available.")

        today = pd.Timestamp(datetime.now().date())
        year_start = pd.Timestamp(datetime(today.year, 1, 1).date())

        self.cleaned_jobs['createDate'] = pd.to_datetime(
            self.cleaned_jobs['createDate']
        )
        self.cleaned_jobs['rental_end_date'] = pd.to_datetime(
            self.cleaned_jobs['rental_end_date']
        )
        self.cleaned_clients['createDate'] = pd.to_datetime(
            self.cleaned_clients['createDate']
        )

        active_orders_mask = self.cleaned_jobs['orderStatus'] == 'Active'
        overdue_returns_mask = (
            (self.cleaned_jobs['rental_end_date'] < today) &
            (self.cleaned_jobs['orderStatus'] != 'Checked In')
        )

        date_points = pd.date_range(start=year_start, end=today)

        active_orders_cumulative = []
        clients_creation_cumulative = []
        overdue_returns_cumulative = []

        for date_point in date_points:
            active_orders_cumulative.append((
                date_point,
                self.cleaned_jobs[
                    (self.cleaned_jobs['createDate'] <= date_point) &
                    active_orders_mask
                ].shape[0]
            ))
            clients_creation_cumulative.append((
                date_point,
                self.cleaned_clients[
                    self.cleaned_clients['createDate'] <= date_point
                ]['clientId'].nunique()
            ))
            overdue_returns_cumulative.append((
                date_point,
                self.cleaned_jobs[
                    (self.cleaned_jobs['rental_end_date'] < date_point) &
                    overdue_returns_mask
                ].shape[0]
            ))

        def format_groupby_result(series):
            return {str(k): v for k, v in series.to_dict().items()}

        self.insights = {
            'timestamp': pd.Timestamp(datetime.now()),
            'active_orders': self.cleaned_jobs[active_orders_mask].shape[0],
            'overdue_rtn': self.cleaned_jobs[overdue_returns_mask].shape[0],
            'clients': self.cleaned_clients['clientId'].nunique(),
            'active_orders_cumulative': active_orders_cumulative,
            'clients_creation_cumulative': clients_creation_cumulative,
            'overdue_returns_cumulative': overdue_returns_cumulative,
            'jobs_due_in_2_days': self.cleaned_jobs[
                self.cleaned_jobs['rental_end_date'] == today +
                pd.Timedelta(days=2)
            ].to_dict('records'),
            'recent_jobs': self.cleaned_jobs.sort_values(
                by='createDate', ascending=False
            ).head(5).to_dict('records'),
            'active_jobs_by_country': format_groupby_result(
                self.cleaned_jobs[active_orders_mask]
                .groupby('countryName').size()
            ),
            'overdue_returns_by_country': format_groupby_result(
                self.cleaned_jobs[overdue_returns_mask]
                .groupby('countryName').size()
            ),
            'clients_by_country': format_groupby_result(
                self.cleaned_clients.groupby('countryName').size()
            ),
            'job_status_by_country': format_groupby_result(
                self.cleaned_jobs.groupby(['countryName',
                                           'orderStatus']).size()
            ),
            'job_status_by_office': format_groupby_result(
                self.cleaned_jobs.groupby(['officeName', 'orderStatus']).size()
            ),
            'top_5_salespeople_by_order_volume_country': format_groupby_result(
                self.cleaned_jobs.groupby(['countryName',
                                           'salesperson']).size()
                .groupby(level=0, group_keys=False).nlargest(5)
            ),
            'top_5_salespeople_by_sales_volume_office': format_groupby_result(
                self.cleaned_jobs.groupby(['officeName', 'salesperson']).size()
                .groupby(level=0, group_keys=False).nlargest(5)
            )
        }

    @staticmethod
    def serialize(obj):
        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        elif isinstance(obj, pd.Timestamp):
            return obj.strftime('%Y-%m-%dT%H:%M:%S')
        elif isinstance(obj, str):
            return obj
        elif isinstance(obj, tuple):
            return str(obj)
        else:
            raise TypeError(f"Type {type(obj)} not serializable")

  
    def save_insights(self):
        if not self.insights:
            raise Exception("Insights not generated.")

        unique_identifier = datetime.now().strftime('%Y%m%d_%H%M%S_%f')
        insights_json = dumps(self.insights, indent=4, default=self.serialize)

        blob_service_client = BlobServiceClient.from_connection_string(
            self.AZURE_STORAGE_CONNECTION_STRING
        )

        blob_client = blob_service_client.get_blob_client(
            container=self.AZURE_CONTAINER_NAME,
            blob=f"insights_{self.timestamp}_{unique_identifier}.json"
        )

        blob_client.upload_blob(
            insights_json,
            content_settings=ContentSettings(content_type='application/json'),
            blob_type='BlockBlob'
        )


if __name__ == "__main__":
    ie_data = IeData()
    ie_data.get_header_with_auth()
    ie_data.get_jobs()
    ie_data.get_clients()
    ie_data.clean_raw_jobs()
    ie_data.clean_raw_clients() 
    ie_data.get_insights()
    ie_data.save_insights()
    print(f"Saved insights to azure blob!!")
    ie_data.save_cleaned_data()
    print(f'Successfully saved data to the database!!')
  