```markdown
# IeData ETL Script

## Overview
This ETL (Extract, Transform, Load) script is designed to interface with the IeLightning API, perform data extraction, transformation, and load the processed data into a SQLite database. It handles data related to jobs and clients, providing insights through data transformation and analysis.

## Features
- Authenticates with the IeLightning API.
- Extracts job and client data.
- Transforms and cleans the data for analysis.
- Stores the cleaned data in a SQLite database.
- Generates insights based on the transformed data.
- Exports insights to JSON format.

## Requirements
- Python 3.x
- Pandas
- SQLite3
- Requests

## Installation
To set up the script on your local environment, follow these steps:
1. Clone the repository or download the script.
2. Install the required dependencies:
   ```bash
   pip install pandas requests
   ```
3. Ensure you have Python 3.x installed.

## Usage
To run the script, perform the following steps:
1. Update the `credentials.txt` with the appropriate API credentials.
2. Execute the script:
   ```bash
   python etl.py
   ```
3. The script will create necessary directories, fetch data, process it, and save the results in the specified format.

## Configuration
- **API Credentials**: Store your IeLightning API credentials in `credentials.txt`.
- **Base URL**: Set the API base URL in the script's `BASE_URL` variable.

## Data Processing
### Extract
- Fetches jobs and client data from the IeLightning API.

### Transform
- Cleans and normalizes the data.
- Converts date formats and filters relevant columns.

### Load
- Stores the data in a SQLite database.
- Exports data to `.csv` and `.json` formats for further analysis.

## Contributing
Contributions to improve the script are welcome. Please follow these steps to contribute:
1. Fork the repository.
2. Create a new branch for your feature.
3. Submit a pull request for review.
---
```
